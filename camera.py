# camera_single.py

import cv2


class Camera:
    def __init__(self, cite):
        print(cite)
        self.video = cv2.VideoCapture(cite)

    def get_frame(self):
        success, image = self.video.read()
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()
