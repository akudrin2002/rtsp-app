from fastapi import FastAPI, Request, Form
from fastapi.responses import StreamingResponse, HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
import multipart
import uvicorn

from camera import Camera

app = FastAPI()
templates = Jinja2Templates(directory="static")
app.mount("/static", StaticFiles(directory="static"), name="static")


url_rtsp = None


@app.get("/", response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse('index.html', {"request": request})


@app.post("/stream")
async def get_stream(request: Request, cite: str = Form()):
    global url_rtsp
    url_rtsp = cite
    return templates.TemplateResponse('stream.html', {"request": request, "cite": cite})


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.get('/video_feed', response_class=HTMLResponse)
async def video_feed():
    return StreamingResponse(gen(Camera(url_rtsp)),
                             media_type='multipart/x-mixed-replace; boundary=frame')
