# Получение ссылки на тестовый rtsp-поток
На [сайте](http://rtsp.stream) после авторизации доступны 2 ссылки

# Построение образа

```bash
sudo docker build -t rtsp-app .
```

# Запуск

```bash
sudo docker run --rm -v "${PWD}/app":/code -p 80:80 rtsp-app
```


