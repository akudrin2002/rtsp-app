FROM dkimg/opencv:4.7.0-alpine

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY . /code/app

WORKDIR /code

CMD ["uvicorn", "app:app", "--reload", "--host", "0.0.0.0", "--port", "80"]
